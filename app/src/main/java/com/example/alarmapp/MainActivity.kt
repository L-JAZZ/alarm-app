package com.example.alarmapp


import android.content.Intent
import android.os.AsyncTask
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.LinearLayout
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.alarmapp.db.Alarm
import com.example.alarmapp.db.AppDatabase
import com.example.alarmapp.ui.AlarmAdapter
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity()  {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        r_view.layoutManager = LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false)
        addAlarms()
        goToNew()
        setupViews()
        refreshApp()
    }

    private fun setupViews() {
        AsyncTask.execute {
            val alarms = AppDatabase.getInstance(applicationContext)!!
                .getAlarmDao().getAlarms()
            runOnUiThread {
                r_view.adapter = AlarmAdapter(alarms)
            }
        }
    }

    private fun addAlarms(){
/*        AsyncTask.execute {
            AppDatabase.getInstance(applicationContext)!!.getAlarmDao().insertAlarm(Alarm(hour = 11, minute = 12, message = "alarm"))
            AppDatabase.getInstance(applicationContext)!!.getAlarmDao().insertAlarm(Alarm(hour = 11, minute = 17, message = "wake up"))
            AppDatabase.getInstance(applicationContext)!!.getAlarmDao().insertAlarm(Alarm(hour = 11, minute = 22, message = "alarm"))
        }*/
    }

    private fun goToNew(){
        goToNew_btn.setOnClickListener {
            val intent = Intent(this,NewAlarmActivity::class.java)
            startActivity(intent)
        }
    }

    private fun refreshApp(){
        swipe.setOnRefreshListener {
            swipe.isRefreshing = false
            setupViews()
        }
    }
}