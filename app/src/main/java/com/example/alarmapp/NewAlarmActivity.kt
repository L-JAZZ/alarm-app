package com.example.alarmapp

import android.os.AsyncTask
import android.os.Build
import android.os.Bundle
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import com.example.alarmapp.db.Alarm
import com.example.alarmapp.db.AppDatabase

import kotlinx.android.synthetic.main.new_alarm_activity.*

class NewAlarmActivity : AppCompatActivity(){

    var hour = 0
    var minute = 0
    var message = ""

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.new_alarm_activity)
        saveTime()
    }

    @RequiresApi(Build.VERSION_CODES.M)
    private fun saveTime(){
        add_btn.setOnClickListener {

            hour = time_picker.hour
            minute = time_picker.minute
            message = message_text.text.toString()
            AsyncTask.execute {
                AppDatabase.getInstance(applicationContext)!!.getAlarmDao().insertAlarm(Alarm(hour = hour, minute = minute, message = message))
            }
            finish()
            Toast.makeText(this.applicationContext,"swipe down to refresh",Toast.LENGTH_SHORT).show()
        }
    }

}