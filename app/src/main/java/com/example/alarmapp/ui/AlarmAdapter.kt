package com.example.alarmapp.ui

import android.os.AsyncTask
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.example.alarmapp.R
import com.example.alarmapp.db.Alarm
import com.example.alarmapp.db.AppDatabase
import kotlinx.android.synthetic.main.alarm_element.view.*

class AlarmAdapter (
    private val alarms: List<Alarm> = listOf()
):RecyclerView.Adapter<AlarmAdapter.AlarmViewHolder>(){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AlarmViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.alarm_element,parent,false)

        return AlarmViewHolder(view)
    }

    override fun onBindViewHolder(holder: AlarmViewHolder, position: Int) {
        holder.bindAlarm(alarms[position])
    }

    override fun getItemCount() = alarms.size

    class AlarmViewHolder(private val view:View):RecyclerView.ViewHolder(view){
        fun bindAlarm(alarm: Alarm){

            view.time_textView_hour.text = alarm.hour.toString()
            if (alarm.hour.toString() == "0"){view.time_textView_hour.text = "00"}
            if (alarm.hour < 10){view.time_textView_hour.text = "0" + alarm.hour.toString()}

            view.time_textView_minute.text = alarm.minute.toString()
            if (alarm.minute.toString() == "0"){view.time_textView_minute.text = "00"}
            if (alarm.minute < 10){view.time_textView_minute.text = "0" + alarm.minute.toString()}


            view.msg_textView.text = alarm.message

            view.setOnClickListener {
                /*AsyncTask.execute {
                    AppDatabase.getInstance()!!.getAlarmDao().deleteAlarm()
                }*/
            }
        }
    }

}
