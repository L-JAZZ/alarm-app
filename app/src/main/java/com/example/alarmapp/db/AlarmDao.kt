package com.example.alarmapp.db

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query

@Dao
interface AlarmDao {
    @Insert
    fun insertAlarm(alarm: Alarm)

    @Query("SELECT * from alarm")
    fun getAlarms(): List<Alarm>

    @Delete
    fun deleteAlarm(alarm: Alarm)
}