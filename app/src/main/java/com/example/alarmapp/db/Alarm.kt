package com.example.alarmapp.db

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class Alarm (
    @PrimaryKey val id: Int? = null,
    @ColumnInfo(name = "hour")val hour: Int,
    @ColumnInfo(name = "minute")val minute: Int,
    @ColumnInfo(name = "message")val message: String,
)